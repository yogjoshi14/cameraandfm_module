# import the necessary packages
from __future__ import print_function
import PIL
from PIL import Image
from PIL import ImageTk
import tkinter as tki
import threading
import datetime
import imutils
import cv2
import os
import subprocess
from imutils import paths
import argparse
import datetime
import numpy as np
#import matplotlib.pyplot as plt
import cv2
import numpy as np
import pywt
#import focus_measures
#from focus_measures import *
#from peakutils.plot import plot as pplot
#from scipy import signal
class PhotoBoothApp:
	def __init__(self, vs, outputPath):
		# store the video stream object and output path, then initialize
		# the most recently read frame, thread for reading frames, and
		# the thread stop event
		self.vs = vs
		self.outputPath = outputPath
		self.frame = None
		self.thread = None
		self.stopEvent = None

		# initialize the root window and image panel
		self.root = tki.Tk()
		self.panel = None

		# create a button, that when pressed, will take the current
		# frame and save it to file
		btn1 = tki.Button(self.root, text="Open Save location",command=self.saveTarget)
		btn1.pack(side="bottom",fill="both")
		btn = tki.Button(self.root, text="Snapshot!",command=self.takeSnapshot)
		btn.pack(side="bottom",fill="both")

		# start a thread that constantly pools the video sensor for
		# the most recently read frame
		self.stopEvent = threading.Event()
		self.thread = threading.Thread(target=self.videoLoop, args=())
		self.thread.start()

		# set a callback to handle when the window is closed
		self.root.wm_title("Micro Camera")
		self.root.wm_protocol("WM_DELETE_WINDOW", self.onClose)

	def videoLoop(self):
		try:
			# keep looping over frames until we are instructed to stop
			while not self.stopEvent.is_set():
				# grab the frame from the video stream and resize it to
				# have a maximum width of 300 pixels
				self.frame = self.vs.read()
				self.frame = imutils.resize(self.frame, width=420)
		
				# OpenCV represents images in BGR order; however PIL
				# represents images in RGB order, so we need to swap
				# the channels, then convert to PIL and ImageTk format
				image = cv2.cvtColor(self.frame, cv2.COLOR_BGR2RGB)
				image = Image.fromarray(image)
				image = ImageTk.PhotoImage(image)
		
				# if the panel is not None, we need to initialize it
				if self.panel is None:
					self.panel = tki.Label(image=image)
					self.panel.image = image
					self.panel.pack(side="left", padx=5, pady=5)
		
				# otherwise, simply update the panel
				else:
					self.panel.configure(image=image)
					self.panel.image = image

		except RuntimeError:
			print("[INFO] caught a RuntimeError")

	def takeSnapshot(self):
		# grab the current timestamp and use it to construct the
		# output path
		ts = datetime.datetime.now()
		filename = "{}.jpg".format(ts.strftime("%Y-%m-%d_%H-%M-%S"))
		p = os.path.sep.join((self.outputPath, filename))

		# save the file
		cv2.imwrite(p, self.frame.copy())
		print("[INFO] saved {}".format(filename))
		oriimg = cv2.imread(p)
		# image = cv2.imread(imagePath)
		#start = datetime.datetime.now()
		image = cv2.resize(oriimg, (480, 360))
		#image = cv2.resize(oriimg , (800, 600))

		gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
		def wave_ratio(img, mwave):
			coeffs2 = pywt.dwt2(img, mwave)
			LL, (LH, HL, HH) = coeffs2
			WH = np.mean(LH*LH + HL*HL + HH *HH)
			WL  =  np.mean(LL)
			FM = WH/WL
			return FM

		fm = wave_ratio(gray, 'haar')  # threshold = 0.20
		threshold = 0.28 # 0.32 #0.39 # 0.43
		print("fm = ", fm)
		# fm = np.max(cv2.convertScaleAbs(fm))
		#end = datetime.datetime.now()
		#elapsed_ms = calc_time(start, end)
		#print("elapsed_ms= ",elapsed_ms)
		text = "focussed"
		if fm < threshold:
			text = "Blurry"
		# show the image

		cv2.putText(image, "{}: {:.2f}".format(text, fm), (10, 30),
		cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 255), 3)
		cv2.imshow("Image", image)
		key = cv2.waitKey(0)

	def saveTarget(self):
		subprocess.Popen(['xdg-open',self.outputPath])
	def onClose(self):
		# set the stop event, cleanup the camera, and allow the rest of
		# the quit process to continue
		print("[INFO] closing...")
		self.stopEvent.set()
		self.vs.stop()
		self.root.quit()

		





